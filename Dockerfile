ARG VERSION

######################################################################
# NODE TOOLS
######################################################################

FROM node:16-alpine as node

LABEL \
    org.opencontainers.image.authors="Rodrigo Buas <rodrigobuas@gmail.com>" \
    org.opencontainers.image.title="CICD Tools - node" \
    org.opencontainers.image.description="CICD Tools containing docker, make and git" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.source="https://gitlab.com/rodrigobuas/cicd-tools:node-${VERSION}" \
    org.opencontainers.image.version="node-${VERSION}"

RUN apk add --no-cache -Uuv \
    git=2.32.0-r0 \
    make=4.3-r0 \
    docker-cli=20.10.11-r0 \
    docker-compose=1.28.0-r1 \
    && rm -rf /var/cache/apk/*


######################################################################
# SLIM TOOLS
######################################################################

FROM alpine:3.14.0 AS slim

LABEL \
    org.opencontainers.image.authors="Rodrigo Buas <rodrigobuas@gmail.com>" \
    org.opencontainers.image.title="CICD Tools - slim" \
    org.opencontainers.image.description="CICD Tools containing docker, make and git" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.source="https://gitlab.com/rodrigobuas/cicd-tools:slim-${VERSION}" \
    org.opencontainers.image.version="slim-${VERSION}"

# hadolint ignore=DL3018
RUN apk add --no-cache -Uuv \
    docker-cli \
    make \
    git


######################################################################
# PLIMB TOOLS
######################################################################

FROM google/cloud-sdk:307.0.0-alpine AS plump

LABEL \
    org.opencontainers.image.authors="Rodrigo Buas <rodrigobuas@gmail.com>" \
    org.opencontainers.image.title="CICD Tools" \
    org.opencontainers.image.description="CICD Tools containing docker, docker-compose, make, git, curl, postgresql and gcp sdk" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.source="https://gitlab.com/rodrigobuas/cicd-tools:plump-${VERSION}" \
    org.opencontainers.image.version="plump-${VERSION}"

# hadolint ignore=DL3018
RUN apk update && apk add --no-cache \
    docker-cli \
    make \
    git \
    openssl-dev \
    gcc \
    libc-dev \
    libffi-dev \
    docker \
    docker-compose \
    curl \
    jq \
    postgresql-client \
    gettext
