git config --global user.email "ci@rbuas.io"
git config --global user.name "rodrigobuas"
url_host=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`

echo "Setting remote as https://gitlab-ci-token:${CI_TOKEN}@${url_host}"
git remote set-url origin "https://gitlab-ci-token:${CI_TOKEN}@${url_host}"
