# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/rodrigobuas/cicd-tools/compare/v0.0.4...v0.0.5) (2021-11-23)

### [0.0.4](https://gitlab.com/rodrigobuas/cicd-tools/compare/v0.0.3...v0.0.4) (2021-11-15)

### [0.0.3](https://gitlab.com/rodrigobuas/cicd-tools/compare/v0.0.1...v0.0.3) (2021-06-19)

### [0.0.2](https://gitlab.com/rodrigobuas/cicd-tools/compare/v0.0.1...v0.0.2) (2021-06-17)

### 0.0.1 (2021-06-16)
