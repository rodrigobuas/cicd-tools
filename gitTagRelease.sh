VERSION_TARGET=${VERSION_TARGET:-patch}

echo "Install yarn packages ..."
yarn install --frozen-lockfile && yarn cache clean

echo "Calculate next version ..."
yarn release --skip.commit --release-as ${VERSION_TARGET}

echo "Read version from package ..."
RELEASE_VERSION=$(./readVersionFromPackage.sh)
echo "RELEASE_VERSION = ${RELEASE_VERSION}"

echo "Saving the CHANGELOG documentation ..."
git add .
git commit -am "📦: version ${RELEASE_VERSION}"

echo "Adding tag v${RELEASE_VERSION} on master ..."
git tag v${RELEASE_VERSION}
git push origin --tags

echo "Merge tagged release into develop ..."
git checkout develop
git merge v${RELEASE_VERSION}
git push origin

echo "Merge tagged release into master ..."
git checkout master
git merge v${RELEASE_VERSION}
git push origin
